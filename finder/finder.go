package finder

import (
	"bitbucket.org/fernandosanchezjr/neurons"
	"os"
	"log"
	"github.com/haochi/blockhash-go"
	"sort"
)

type Finder struct {
	*neurons.Network
}

func NewFinder() *Finder {
	f := &Finder{Network: neurons.NewNetwork()}
	f.Setup()
	return f
}

func (f *Finder) Setup() {

}

func (f *Finder) ImageHash(file string, bits int) []int {
	reader, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}
	hash, _ := blockhash.Blockhash(reader, bits)
	return hash.Bits
}

func (f *Finder) normalize(min float64, width float64, value uint16) float64 {
	return min + float64(value)*(width)/float64(65535)
}

func (f *Finder) InputsFromHash(hash []int) []float64 {
	inputs := make([]float64, 0)
	for ; len(hash) > 0; {
		var num uint16
		var h []int
		sort.Reverse(sort.IntSlice(hash))
		if len(hash) > 16 {
			h, hash = hash[:16], hash[16:]
		} else {
			h = hash
			hash = []int{}
		}
		for i, bit := range h {
			if bit == 1 {
				num |= 1 << uint(i)
			}
		}
		inputs = append(inputs, f.normalize(-1.0, 2.0, num))
	}
	return inputs
}

func (f *Finder) InputsFromImage(file string, bits int) []float64 {
	h := f.ImageHash(file, bits)
	return f.InputsFromHash(h)
}
package finder

import (
	"testing"
	"time"
	"math/rand"
	"bitbucket.org/fernandosanchezjr/neurons"
	"bitbucket.org/fernandosanchezjr/bus"
)

func TestNewFinder(t *testing.T) {
	f := NewFinder()
	if f == nil {
		t.FailNow()
	}
}

// ripped off from
// https://www.socketloop.com/tutorials/golang-how-to-shuffle-elements-in-array-or-slice
func shuffle(arr []int) {
	t := time.Now()
	rand.Seed(int64(t.Nanosecond())) // no shuffling without this line

	for i := len(arr) - 1; i > 0; i-- {
		j := rand.Intn(i)
		arr[i], arr[j] = arr[j], arr[i]
	}
}

func getClientIds(neuronIds []int, t *testing.T, searchFun func(int) ([]int, error)) []int {
	clientIds := make([]int, 0)
	for _, h := range neuronIds {
		if cs, err := searchFun(h); err != nil {
			t.Fatal(err)
		} else {
			clientIds = append(clientIds, cs...)
		}
	}
	return clientIds
}

func setupHiddenA(t *testing.T, f *Finder, external []int, hiddenA []int) {
	hiddenClientIds := getClientIds(hiddenA, t, f.GetHiddenClientIds)

	shuffle(external)
	shuffle(hiddenClientIds)

	var nextExternal, nextHidden int
	for ; len(hiddenClientIds) > 0; {
		nextExternal, external = external[0], external[1:]
		nextHidden, hiddenClientIds = hiddenClientIds[0], hiddenClientIds[1:]
		f.Connect(nextExternal, nextHidden)
	}
}

func setupHiddenB(t *testing.T, f *Finder, hiddenA []int, hiddenB []int) {
	hiddenClientBIds := getClientIds(hiddenB, t, f.GetHiddenClientIds)

	hiddenACopy := hiddenA
	for ; len(hiddenACopy) < len(hiddenClientBIds); {
		hiddenACopy = append(hiddenACopy, hiddenA...)
	}
	shuffle(hiddenACopy)
	shuffle(hiddenClientBIds)

	var nextA, nextB int

	for ; len(hiddenClientBIds) > 0; {
		nextA, hiddenACopy = hiddenACopy[0], hiddenACopy[1:]
		println(hiddenClientBIds[0], hiddenClientBIds[1:])
		nextB, hiddenClientBIds = hiddenClientBIds[0], hiddenClientBIds[1:]
		f.Connect(nextA, nextB)
	}
}

func setupHiddenC(t *testing.T, f *Finder, hiddenB []int, hiddenC []int) {
	hiddenClientCIds := getClientIds(hiddenC, t, f.GetHiddenClientIds)

	hiddenBCopy := hiddenB
	for ; len(hiddenBCopy) < len(hiddenClientCIds); {
		hiddenBCopy = append(hiddenBCopy, hiddenB...)
	}
	shuffle(hiddenBCopy)
	shuffle(hiddenClientCIds)

	var nextB, nextC int

	for ; len(hiddenClientCIds) > 0; {
		nextB, hiddenBCopy = hiddenBCopy[0], hiddenBCopy[1:]
		println(hiddenClientCIds[0], hiddenClientCIds[1:])
		nextC, hiddenClientCIds = hiddenClientCIds[0], hiddenClientCIds[1:]
		f.Connect(nextB, nextC)
	}
}

func setupOutput(t *testing.T, f *Finder, hiddenC []int, output []int) {
	outputClientIds := getClientIds(output, t, f.GetOutputClientIds)

	hiddenCCopy := hiddenC
	for ; len(hiddenCCopy) < len(outputClientIds); {
		hiddenCCopy = append(hiddenCCopy, hiddenC...)
	}
	shuffle(hiddenCCopy)
	shuffle(outputClientIds)

	var nextB, nextC int

	for ; len(outputClientIds) > 0; {
		nextB, hiddenCCopy = hiddenCCopy[0], hiddenCCopy[1:]
		println(outputClientIds[0], outputClientIds[1:])
		nextC, outputClientIds = outputClientIds[0], outputClientIds[1:]
		f.Connect(nextB, nextC)
	}
}

func setupTestFinder(t *testing.T, f *Finder) {
	for i := 0; i < 8; i++ {
		f.AddExternal(2)
	}
	for i := 0; i < 14; i++ {
		f.AddHidden(2)
	}
	f.AddOutput(4)
	hiddenIds :=  f.GetHiddenIds()
	hiddenA := hiddenIds[0:4]
	hiddenB := hiddenIds[4:10]
	hiddenC := hiddenIds[10:14]

	setupHiddenA(t, f, f.GetExternalIds(), hiddenA)

	setupHiddenB(t, f, hiddenA, hiddenB)

	setupHiddenC(t, f, hiddenB, hiddenC)

	setupOutput(t, f, hiddenC, f.GetOutputIds())
}

func TestInputsFromImage(t *testing.T) {
	f := NewFinder()
	setupTestFinder(t, f)
	inputChannels, err := f.GetAllExternalClients()
	if err != nil {
		t.Fatal(err)
	}
	inputs := f.InputsFromImage("cat1.jpg", 16)
	tmpInputChannels := inputChannels
	var next *bus.Client
	for _, v := range inputs {
		next, tmpInputChannels = tmpInputChannels[0], tmpInputChannels[1:]
		next.Message(neurons.BaseMessage{Id: int64(next.Id), Frame: 0, Value: v})
	}
	if len(inputs) == 0 {
		t.FailNow()
	}
	f.Wait()
}
